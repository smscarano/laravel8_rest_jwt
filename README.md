14/5/21
## Laravel based Users Restful API

## Description
Restful JSON API powered by Laravel.
It is intended to be used as a skeleton app and build on top.
Comes with users resource.
 
## Requirements
 - php 7.*
 - php-intl
 - php-mbstrimg
 - composer
 - Apache2 mod rewrite
 - mysql server
 - git
 
## Install

`git clone git@bitbucket.org:smscarano/laravel8_rest_jwt.git`  
`cd laravel8_rest_jwt`  
`composer install`  
`php artisan migrate`  