<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Temperature;

class TemperatureController extends Controller
{
    public function create(Request $request)
    {
            $validator = Validator::make($request->all(), [
            'sensor_id' => 'required|string|max:255',
            'value_celcius' => 'required|min:2',
        ]);

        if($validator->fails()){
                return response()->json($validator->errors()->toJson(), 400);
        }

        $temperature_value = Temperature::create([
            'sensor_id' => $request->get('sensor_id'),
            'value_celcius' => $request->get('value_celcius')
        ]);

        return response()->json(array('status'=>'ok'));
    }
}
